import { sessionService } from 'redux-react-session';
import { toast } from 'react-toastify';

class Session {
    async save(data) {
        let previousData = await this.load()
        previousData.session_data = Object.assign(previousData.session_data, data)
        return await sessionService.saveSession(previousData)
    }

    async load () {
        let result = await sessionService.loadSession()

        return result
    }

    async getSessionId() {
        let data = await this.load()
        return data.session_data.session_id
    }

    killSession() {
        sessionService.loadSession().then((res) => {
            res.invalid = true
            sessionService.saveSession(res).then(() => {
                toast.error('Invalid Session!')
                this._user = {}
                window.location.hash = "login"
            })
        }).catch((err) => {
            console.log(err)
        })
    }

}
export default Session;