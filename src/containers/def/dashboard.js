import React, { Component } from 'react';
import { Row, Col } from 'reactstrap';

import Daily from './dashboard/daily'



class Dashboard extends Component {
    render() {
        return (
            <Row>
                <Col className="dashboard">
                    <div className="dashboard-header">
                        <h2>Welcome, Today is {this.current_date}. This is the general status of configured APIs:</h2>
                        <hr></hr>
                    </div>
                    <div className="dashboard-content">
                        <Daily/>
                    </div>
                </Col>
            </Row>
        );
    }

    get current_date() {
        let date = new Date()
        return date.toLocaleDateString('en-US', { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' })

    }
}

export default Dashboard;