import React, { Component } from 'react';
import { Col, Row, Collapse, Button, CardBody, CardHeader, Card, FormGroup, Label, Input } from 'reactstrap';
import { ClipLoader } from 'react-spinners'
import { css } from 'react-emotion';
import Bar from './config/bar.js'
import { toast } from 'react-toastify';
import Session from '../../session'

class Config extends Component {
    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        this.fieldChange = this.fieldChange.bind(this);
        this.save = this.save.bind(this);
        this._sessionManager = new Session()
        this.state = { 
            collapse: [],
            loading: true,
            sections : {}
        };
        this._sections = []
        this._configuration = {}
        this._sessionManager.getSessionId().then((id) => {
            this._sesionId = id
            this.getConfiguration()
        })
    }

    getConfiguration() {
        fetch(this.queryStringInfo(), {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'X-Api-Manager-Session' : this._sesionId
                },
                dataType: 'json',
            credentials: 'same-origin',
        }).then((response)=>{
            if (response.status === 403) {
                this._sessionManager.killSession()
            }
            if (response.status === 200) {
                response.json().then((data) => {
                    this._sections = data
                    let newSec = {}
                    for (let section of this._sections) {
                        newSec[section.id] = {}
                    }
                    this.setState({sections : newSec})
                    this.state.loading = false
                    this.forceUpdate()
                })
            }
        })
    }

    getSectionValues(section, callback) {
        if (this._configuration[section]) {
            console.log(this._configuration)
            return callback()
        }
        this.state.sections[section].loading = true
        this.forceUpdate()
        fetch(this.queryStringData(section), {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'X-Api-Manager-Session' : this._sesionId
                },
                dataType: 'json',
            credentials: 'same-origin',
        }).then((response)=>{
            if (response.status === 200) {
                response.json().then((data) => {
                    this.state.sections[section].loading = false
                    this._configuration[section] = data
                    this.appendValues(section)
                    this.forceUpdate();
                    callback()
                })
            }
        })
        
        
    }
    
    toggle(ev) {
        let sectionClicked = ev.target.id ? ev.target.id : ev.target.dataset.section;
        this.getSectionValues(sectionClicked, () => {
            if ( this.state.collapse.includes(sectionClicked)) {
                this.state.collapse = this.state.collapse.filter(section => section != sectionClicked)
            }else {
                this.state.collapse.push(sectionClicked)
            }
            this.setState({collapse : this.state.collapse})    
        })

    }

    queryStringInfo() {
        return global.config.api_url+global.config.config
    }

    queryStringData(section) {
        return global.config.api_url+global.config.config_value+"?section="+section
    }

    prepareSection(section) {

        return (
            
            <Col xl="6"data-section={section.id}>
            
                <CardHeader onClick={this.toggle} data-section={section.id} className="clickable">
                    <Button id={section.id} color="link">
                        {section.label}
                    </Button>
                    <ClipLoader
                        sizeUnit={"px"}
                        size={20}
                        color={'#123abc'}
                        loading={this.state.sections[section.id].loading == true}
                    />
                </CardHeader>
                <Collapse isOpen={this.state.collapse.includes(section.id)}>
                    {section.groups.map(group => this.prepareGroup(group, section))}
                </Collapse>
            </Col>
        )
    }

    prepareGroup(group, section) {
        return (
            <Card>
                <CardHeader>{group.label}</CardHeader>
                <CardBody>
                {group.fields.map(field => this.prepareField(field, group, section))}
                </CardBody>
            </Card>
        )
    }

    fieldChange(ev) {
        ev.target.classList.add("element-changed")
    }

    prepareField(field, group, section) {
        let name = section.id+"/"+group.id+"/"+field.id
        switch (field.type) {
            case 'text':
                return (
                    <FormGroup row>
                        <Label for={name}>{field.label}</Label>
                        
                            <Input type={field.type} name={name} id={name} placeholder={field.label} onChange={this.fieldChange}/>
                        
                    </FormGroup>
                )
            case 'select':
                let Options = field.options.map((item) => {
                    return (
                        <option value={item.value}>{item.label}</option>
                    )
                })
                return (
                    
                    <FormGroup row>
                        <Label for={field.id}>{field.label}</Label>
                            <Input type="select" name={name} id={name} onChange={this.fieldChange}>
                                {Options}
                            </Input>
                    </FormGroup>
                )
        }
    }
    
    render() {
        if (this.state.loading) {
            return this._loading()
        }
        return (
            <div>
                <Row className="config-container">
                    <Col xl >
                        <Bar title="Configuration" onSave={this.save}/>
                    </Col>
                </Row>
                <Row className="config-container config-section-container">
                    {this._sections.map((section) => {
                        return this.prepareSection(section)
                    })}
                </Row>
            </div>
        );
    }
    _loading() {
        const override = css`
            display: block;
            margin: 0 auto;
            border-color: red;
        `;

        return (
            <div className='sweet-loading'>
              <ClipLoader
                className={override}
                sizeUnit={"px"}
                size={50}
                color={'#123abc'}
                loading={this._isLoaded}
              />
            </div> 
          )
    }
    appendValues(section) {
        let data = this._configuration[section]
        console.log(data)
        for (let config of data) {
            let ele = document.getElementById(config.path)
            if (ele){
                if ( config.value === true ) {
                    ele.value = 1
                }
                else if (config.value === false) {
                    ele.value = 0
                }
                else {
                    ele.value = config.value
                }
            }
            
        }
    }

    save(ev, callback) {
        let fields = document.getElementsByClassName("element-changed")
        if (fields.length == 0) {
            callback()
            return 
        }
        let data = []
        for (let field of fields) {
            data.push({
                'path' : field.name,
                'value' : field.value
            })
        }

        fetch(global.config.api_url+global.config.config_value, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'X-Api-Manager-Session' : this._sesionId
                },
                dataType: 'json',
            body:JSON.stringify(data),
            credentials: 'same-origin',
        }).then((response)=>{
            if (response.status === 200) {
                toast.success("Configuration saved succesfully")
            }
            else {
                toast.error("Could not save configuration")
            }
            callback()
            
        })
    }
}

export default Config;