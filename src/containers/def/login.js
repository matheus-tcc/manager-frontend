import React, { Component } from 'react';
import '../../login.css';
import { toast } from 'react-toastify';
import { sessionService } from 'redux-react-session';

class Login extends Component {
    constructor() {
        super()
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleSubmit(event) {
        event.preventDefault();
        
        let data = {
            username : event.target[0].value,
            password : event.target[1].value,
        }        
        
        fetch(this.routeToRequest, {
            method: 'PUT',
            headers: {
                'Accept': 'application/json'
                },
                dataType: 'json',
            credentials: 'same-origin',
            bodyUsed: true,
            body: JSON.stringify(data),
        }).then(response => {
            if (response.status === 500) {
                response.json().then((res) => {
                    toast.error(res.message)
                })
            }else {
                toast.success('API Manager Ready!')
                response.json().then((res) => {
                    sessionService.saveSession({ 
                        session_data : {
                            session_id:res.sessionId,
                            username:data.username
                        }
                    }).then(() => {
                        window.location.hash = "home"
                        this.forceUpdate()
                    }).catch(error => console.log(error))
                })
            }
        });
    }

    get routeToRequest() {
        return global.config.api_url+global.config.login_route
    }


    render() {
        return (
            <div className="login-page">
            <div className="form">
                <form className="login-form" onSubmit={this.handleSubmit}>
                <input type="text" placeholder="username" required/>
                <input type="password" placeholder="password" required/>
                <button>login</button>
                </form>
            </div>
            </div>
        )
    }
}

export default Login