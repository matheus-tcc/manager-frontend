import React, { Component } from 'react';
import { Card, Col,CardTitle } from 'reactstrap';
import { Bar } from 'react-chartjs-2';
import Session from '../../../session'
import { ClipLoader } from 'react-spinners';
import randomColorRGB from 'random-color-rgb'

class Daily extends Component {
    constructor(props){
        super(props)
        this.state = {}
        this.session = new Session()
        this.setState({
            loading : true,
            chartData : null
        })
        this.loadChart()
    }
    get routeToRequest() {
        return global.config.api_manager_url+global.config.stats+"/daily"
    }
    /**
     * Loads chart data (Ajax request)
     */
    loadChart() {
        this.session.getSessionId().then((id) => {
            fetch(this.routeToRequest, {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'X-Api-Manager-Session' : id
                    },
                    dataType: 'json',
                credentials: 'same-origin',
            }).then((response)=>{
                if (response.status === 403) {
                    this.session.killSession()
                }
                if (response.status == 200) {
                    response.json().then((data) => {
                        this.setState({
                            loading : false,
                            chartData : data
                        })
                        this.forceUpdate()
                    })
                }
            })
        })
    }
    /**
     * Getter for chart data
     * @returns Object
     */
    get chart () {
        let format = (date) => {
            return date.getUTCDate()+"/"+date.getUTCMonth()+"/"+date.getUTCFullYear()
        }
        let data = this.state.chartData
        let result = {

        }
        let consumers = []
        if (data) {
            result.labels = data.map(req => format(new Date(req.date)))
            data.map((d) => {
                d.requests.map((info) => {
                    let found = false
                    for (let key in consumers) {
                        if(consumers[key].label == info.consumer){
                            consumers[key].data.push(info.quantity)
                            found = true
                            break
                        }
                    }
                    if (!found) {
                        consumers.push({
                            label : info.consumer,
                            data : [info.quantity],
                            backgroundColor : randomColorRGB({opacity: 0.8})
                        })
                    }
                })
                for (let key in consumers) {
                    if (!d.requests.find(ele => ele.consumer == consumers[key].label)) {
                        consumers[key].data.push(0)
                    }
                }
            })
            result.datasets = consumers
        }
        return result
            
    }

    render() {
        if (this.state.loading) {
            return (<Col md="4">
                        <Card body>
                            <ClipLoader/>
                        </Card>
                    </Col>
            )
        }
        return (
            <Col sm="4">
                <Card body>
                    <CardTitle>Daily requests</CardTitle>
                    <Bar ref="daily_req" data={this.chart} width="450" height="250"/>
                </Card>
            </Col>
        );
    }
}

export default Daily;