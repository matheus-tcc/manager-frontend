import React, { Component } from 'react';
import {NavItem, NavLink} from 'reactstrap'
import { ClipLoader } from 'react-spinners';
import Session from '../../session'



class MenuContainer extends Component {
    constructor() {
       super();
       this.menu_items = []
       this._sessionManager = new Session()
    }

    get current_menu () {
        for(let item of this.menu_items) { 
            if (window.location.hash.indexOf(item.path) !== -1) {
                return item.id
            }
        }

        return ''
    }

    render() {
        if (this.menu_items.length == 0) { //we have no items to render
            return this._loading()
        }
        else {

            return (
                <React.Fragment>
                    {this.menu_items.map((item, key) => {
                        if (item.id !== 'dashboard') {
                            return (
                                <NavItem key={key}>
                                    <NavLink href={'#'+item.path}>{item.label}</NavLink>
                                </NavItem>
                            )
                            
                        }
                    })}
                </React.Fragment>
            );
        }


    }
    get routeToRequest() {
        return global.config.api_url+global.config.menu_route
    }

    componentWillMount() {
        this._sessionManager.getSessionId().then((id) => {
            fetch(this.routeToRequest, {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'X-Api-Manager-Session' : id
                    },
                    dataType: 'json',
                credentials: 'same-origin',
            }).then((response)=>{
                if (response.status === 403) {
                    this._sessionManager.killSession()
                }
                if (response.status == 200) {
                    response.json().then((data) => {
                        this.menu_items = data
                        this.forceUpdate()
                    })
                }
            })
        })
    }

    _loading() {
        return (
            <React.Fragment>
                <div className="menu-container" style={{background: 'radial-gradient(#41a3ff, #0273D4)', color: '#FFF', width: 220}}> 
                    <ClipLoader/>
                </div>
            </React.Fragment>
            )
    }
}
  
  export default MenuContainer