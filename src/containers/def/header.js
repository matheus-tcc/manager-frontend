import React, { Component } from 'react';
import {Navbar, NavbarBrand, NavbarToggler, Nav, UncontrolledDropdown, DropdownToggle, DropdownItem, DropdownMenu, Collapse} from 'reactstrap'

import { toast } from 'react-toastify';

import { sessionService } from 'redux-react-session';

import Menu from './menu'

class Header extends Component {
    constructor(props) {
        super(props)
        this._user = {}
        this.state = {
            isOpen: false
        }
        this.toggle = this.toggle.bind(this);
        this.goToConfig = this.goToConfig.bind(this);
        this._logout = this._logout.bind(this);
    }
    goToConfig(ev) {
        window.location.hash = "config"
    }
    componentWillMount() {
        sessionService.loadSession().then((res) => {
            this._user = res.session_data
            global.session_data = res.session_data
            this.forceUpdate()
        }).catch((err) => {
            this.sessionObj = null
            console.log(err)
        })
    }
    toggle() {
        this.setState({
          isOpen: !this.state.isOpen
        });
    }
    render() {
      return (
        <div>
            <Navbar color="dark" dark expand="md">
                <NavbarBrand href="/#">Api Manager</NavbarBrand>
                <NavbarToggler onClick={this.toggle} />
                <Collapse isOpen={this.state.isOpen} navbar>
                    <Nav className="ml-auto" navbar>
                        <Menu />
                        <UncontrolledDropdown nav inNavbar>
                            <DropdownToggle nav caret>
                            {this._user.hasOwnProperty('username') ? this._user.username : ''}
                            </DropdownToggle>
                            <DropdownMenu right>
                                <DropdownItem onClick={this.goToConfig}>Configurations</DropdownItem>
                                <DropdownItem onClick={this._logout}>Logout</DropdownItem>
                            </DropdownMenu>
                        </UncontrolledDropdown>
                    </Nav>
                </Collapse>
            </Navbar>
        </div>
      );
    }

    _logout(ev) {
        ev.preventDefault();
        sessionService.loadSession().then((res) => {
            res.invalid = true
            sessionService.saveSession(res).then(() => {
                toast.success('Logged out successfully!')
                this._user = {}
                window.location.hash = "login"
            })
        }).catch((err) => {
            console.log(err)
        })
    }
  }
  
  export default Header;