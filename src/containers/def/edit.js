import React, { Component } from 'react';
import { Col, Button, Form, FormGroup, Label, Input, TabContent, TabPane, Nav, NavItem, NavLink, Row } from 'reactstrap';
import { css } from 'react-emotion';
import { ClipLoader } from 'react-spinners'
import History from './edit/history'
import { toast } from 'react-toastify';
import classnames from 'classnames';
import Session from '../../session'
import TimePicker from 'rc-time-picker';
import moment from 'moment';
import 'rc-time-picker/assets/index.css';


const default_fields = ['text', 'select', 'array', 'time']


class Edit extends Component {
    constructor(props, context) {
        super(props, context)
        this._formInfo = null
        this._itemData = null
        this.toggle = this.toggle.bind(this);
        this._sessionManager = new Session()
        this.state = {
            activeTab: '1',
            loading: true,
            delay_from : moment(),
            delay_to: moment()
        }

        this._specialFields = []
        this.tabs = []
        this._optionalFields = []
    }

    componentWillMount() {
        this._sessionManager.getSessionId().then((id) => {
            this._sesionId = id
            this.prepareFormInfo()
        })
    }

    setFormInfo(info) {
        this._formInfo = info
    }
    prepareFormInfo() {
        fetch(this.queryStringInfo(), {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'X-Api-Manager-Session' : this._sesionId
                },
                dataType: 'json',
            credentials: 'same-origin',
        }).then((response)=>{
            if (response.status === 403) {
                this._sessionManager.killSession()
            }
            if (response.status === 200) {
                response.json().then((data) => {
                    this.setFormInfo(data)
                    for (let field of this._formInfo.fields) {
                        if (!default_fields.includes(field.type)) {
                            this._specialFields.push(field)
                        }
                    }
                    this.tabs.push({
                        type : 'form',
                        title:'Edit/Create',
                        content: () => {return this.getForm()}
                    })
                    if (this.current_data_id) {
                        this.getItemData()
                    }
                    else {
                        this._itemData = {}
                        this.setState({loading:false})
                    }
                })
            }
        })
    }
    getItemData() {
        fetch(this.queryStringData(), {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'X-Api-Manager-Session' : this._sesionId
                },
                dataType: 'json',
            credentials: 'same-origin',
        }).then((response)=>{
            if (response.status === 200) {
                response.json().then((data) => {
                    this._itemData = data
                    console.log(this._specialFields)
                    if( this._specialFields.length > 0 ) {
                        this.addTabFromSpecialField();
                    }
                    this.setState({loading:false})
                    this.appendValues()
                        
                })
            }
        })
    }

    addTabFromSpecialField() {
        let data = this._itemData.data
        for ( let field of this._specialFields) {
            switch ( field.type ) {
                case 'history' : 
                    if (data.hasOwnProperty(field.name)) {
                        this.tabs.push({
                            title : 'History',
                            content : () => {return this.getHistory()}
                        })
                    }
                break
            }

        }
    }
    isFieldTime(fieldName) {
        for (let field of this._formInfo.fields) {
            if (field.name == fieldName && field.type == 'time') {
                return true
            }
        }
        return false
    }
    appendValues() {
        
        let data = this._itemData.data
        for (let key in data) {
            let ele = document.getElementById(key)
            if (this._optionalFields.includes(key)) {
                ele.parentElement.parentElement.classList.remove('d-none')
            }
            if (ele){
                if ( data[key] === true ) {
                    ele.value = 1
                }
                else if (data[key] === false) {
                    ele.value = 0
                }
                else if (this.isFieldTime(key)) {
                    let date = new Date(data[key])
                    let st = {}
                    st[key] = moment().hour(date.getUTCHours()).minute(date.getMinutes())
                    this.setState(st)
                }
                else {
                    ele.value = data[key]

                }
            }   
        }
    }

    queryStringInfo() {
        return `${this.route_info}?form_id=${this.current_data_type}`;
    }

    queryStringData() {
        return `${this.route_data}?id=${this.current_data_id}`;
    }

    get route_info() {
        return global.config.api_url+global.config.form_info
    }

    get route_data() {
        return global.config.api_manager_url+this.current_data_type
    }

    get current_data_type() {
        let splitStr = window.location.hash.split('/')
        for (let key in splitStr) {
            if (splitStr[key] == 'entity') {
                return splitStr[parseInt(key)+1]
            }
        }

        return "";
    }

    get current_data_id() {
        let splitStr = window.location.hash.split('/')
        for (let key in splitStr) {
            if (splitStr[key] == 'id') {
                return splitStr[parseInt(key)+1]
            }
        }

        return false;
    }

    onChange(time, el) {
        let st = {}
        st[el] = time
        this.setState(st)
    }

    prepareField(field, key) {
        let eleClass = ''
        if (field.optional) {
            eleClass = 'd-none'
            this._optionalFields.push(field.name)
        }
        console.log(field)
        switch (field.type) {
            case 'text':
                return (
                    <FormGroup row className={eleClass} key={key}>
                        <Label for={field.name}>{field.label}</Label>
                        
                        <Input type={field.type} name={field.name} id={field.name} placeholder={field.label}/>
                        
                    </FormGroup>
                )
            case 'select':
                let Options = field.options.map((item, k) => {
                    return (
                        <option key={k} value={item.value}>{item.label}</option>
                    )
                })
                return (
                    
                    <FormGroup row className={eleClass} key={key}>
                        <Label for={field.name}>{field.label}</Label>
                        <Input type="select" name={field.name} id={field.name}>
                            {Options}
                        </Input>
                    </FormGroup>
                )
            case 'array':
            console.log(key, field)
                return (
                    <FormGroup row className={eleClass} key={key}>
                        <Label for={field.name}>{field.label}</Label>
                        <Input type="text" name={field.name+"[]"} id={field.name} placeholder={field.label} className="array-field"/>
                    </FormGroup>
                )
            case 'time' :
                    return (
                        <FormGroup row className={eleClass} key={key}>
                            <Label for={field.name}>{field.label}</Label>
                            <TimePicker
                                name={field.name}
                                id={field.name}        
                                showSecond={false}
                                value={this.state[field.name]}
                                className="form-control"
                                onChange={(time) => { this.onChange(time, field.name)}}
                            />
                        </FormGroup>

                    )
            default:
                
                break

        }
    }
    save() {
        var serialize = require('form-serialize')
        let method = 'POST'
        let route = this.route_data
        if ( this.current_data_id ) {
            method = 'PUT'
            route = route+"?id="+this.current_data_id
        } 
        fetch(route, {
            method: method,
            headers: {
                'Accept': 'application/json',
                'X-Api-Manager-Session' : this._sesionId
                },
                dataType: 'json',
            credentials: 'same-origin',
            bodyUsed: true,
            body: JSON.stringify(serialize(document.querySelector('#edit-form'), { hash: true, serializer: (result,key,value) => {
                if (key.indexOf("[]") !== -1) {
                    if (value.indexOf(',') !== -1) 
                        result[key.substr(0, key.indexOf("["))] = value.split(",").map(v => v.trim())
                    else 
                        result[key.substr(0, key.indexOf("["))] = [value]
                }
                else if (this.isFieldTime(key)) {
                    let time = value.split(':')
                    result[key] = new Date()
                    result[key].setUTCHours(time[0])
                    result[key].setMinutes(time[1])
                }
                else {
                    result[key] = value
                }
                return result
            } })),
        }).then((response) => {
            if (response.status == 500) {
                response.json().then((res) => {
                    toast.error(res.message)
                })
            }else {
                response.json().then((resp) => {
                    toast.success('Saved Successfully')
                })
            }
        })
    }
    toggle(tab) {
        if (this.state.activeTab !== tab) {
          this.setState({
            activeTab: tab
          });
        }
      }

    getNav() {
        return (
        <Nav tabs className="edit-form">
            {this.tabs.map((tab, index) => {
                return (
                    <NavItem key={index}>
                        <NavLink
                            className={classnames({ active: this.state.activeTab === index })}
                            onClick={() => { this.toggle(index); }}
                        >
                        {tab.title}
                        </NavLink>
                    </NavItem>
                )        
            })}   
        </Nav>
        )
    }

    getTabContent() {
        return (
            <TabContent activeTab={this.state.activeTab} >
                {this.tabs.map((value, key) => {
                    return (
                        <TabPane tabId={key} key={key}>
                            {value.content()}
                        </TabPane>
                    )
                })}
                
            </TabContent>
        )
    }
    render() {
        if (!this._formInfo || !this._itemData) {
            return this._loading()
        }
        return (
            <div>
                {this.getNav()}
                {this.getTabContent()}
            </div>
        );
    }
    componentDidMount() {
        this.toggle(0);
    }

    getForm () {
        return (
            <Row className="edit-form">
                <div className="form-inner-container">
                    <div className="edit-form-header">
                            <h3>{this._formInfo.title}</h3>
                            <hr></hr>
                    </div>
                    <div className="edit-form-content">
                        <Form id="edit-form">
                            {this._formInfo.fields.map((field, key) => {
                                return this.prepareField(field, key)
                            })}
                            <Button onClick={(ev) => {this.save()}}>Submit</Button>
                        </Form>
                    </div>
                </div>
            </Row>
        )
    }

    getHistory() {
        return (<History data={this._itemData}/>)
    }

    _loading() {
        const override = css`
            display: block;
            margin: 0 auto;
            border-color: red;
        `;

        return (
            <div className="edit-form">
                <div className='sweet-loading'>
                <ClipLoader
                    className={override}
                    sizeUnit={"px"}
                    size={50}
                    color={'#123abc'}
                    loading={this._isLoaded}
                />
                </div> 
            </div>
          )
    }
}

export default Edit;