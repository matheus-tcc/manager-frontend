import React, { Component } from 'react';
import { Grid, Table, TableHeaderRow, PagingPanel, TableRowDetail} from '@devexpress/dx-react-grid-bootstrap4';
import { Col} from 'reactstrap';
import {
    PagingState,
    IntegratedPaging,
    RowDetailState,
    DataTypeProvider
  } from '@devexpress/dx-react-grid';
import 'open-iconic/font/css/open-iconic-bootstrap.css'
const Entities = require('html-entities').XmlEntities;  

class HistoryGrid extends Component {
    constructor(props) {
        super(props)
        this.title = 'Last Requests'
        this.data = props.data
        this._rows = []
        this.data.map((da) =>{
            this._rows = Object.assign(this._rows, da.requests)
        })
        
        this._rows = this._rows.sort((a, b) => {
            let aDate = new Date(a.datetime)
            let bDate = new Date(b.datetime)
            return aDate < bDate
        })
        this.state = {
            totalCount: this._rows.length,
            pageSize: 10,
            currentPage: 0,
            loading: true,
        }
        this.changeCurrentPage = this.changeCurrentPage.bind(this)
    }
    get columns() {
        if (!this._columns) {
            let keys = []
            this.data.map((d) => {
                d.requests.map((req) =>{
                    keys = Object.assign(keys, Object.keys(req))
                })
            })
            keys = keys.filter(v => v !== "body")
            this._columns = keys.map((key) => {
                return {
                    name:key, 
                    title:key
                }
            })
        }
        return this._columns
    }
    changeCurrentPage(currentPage) {
        this.setState({
          loading: true,
          currentPage : currentPage,
        });
        this.forceUpdate()
    }
    render() {
        var entities = new Entities();
        var prettyHtml = require('json-pretty-html').default;
        const {
            pageSize, currentPage, totalCount
        } = this.state;
        const RowDetail = (row) => {
            row = row.row
            let body = typeof row.body === "object" ? prettyHtml(row.body) : entities.encode(row.body)
            return (
                <div>
                    <strong>Request:</strong>
                    <br></br>
                    <code dangerouslySetInnerHTML={{__html:body}}>
                    </code>
                </div>
            )
        };
     
        const BooleanFormatter = ({ value }) => (
            <span className="badge badge-secondary">
              {value ? 'Yes' : 'No'}
            </span>
        );

        const BooleanTypeProvider = props => (
        <DataTypeProvider
            formatterComponent={BooleanFormatter}
            {...props}
        />
        );
        return (
            <React.Fragment>
                <Col>
                    <div className="over-grid">
                        <div className="title-container">
                            <h3 className="grid-title">{this.title}</h3>
                        </div>
                    </div>
                    <div className="grid-inner-container table-striped">
                        <Grid
                        rows={this._rows}
                            columns={this.columns}>
                            <PagingState
                                defaultCurrentPage={0}
                                pageSize={10}
                                />
                                <BooleanTypeProvider
                                    for={['delayed']}
                                />
                                <RowDetailState/>
                                <IntegratedPaging />
                                <Table />
                                <TableHeaderRow />
                                <TableRowDetail
                                    contentComponent={RowDetail}
                                />
                                <PagingPanel  />
                            </Grid>
                        </div>
                    </Col>
            </React.Fragment>
        )        
    }
}

export default HistoryGrid