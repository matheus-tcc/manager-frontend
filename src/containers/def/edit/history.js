import React, { Component } from 'react';
import { Col, TabContent, TabPane, Card, CardTitle, CardText, Row } from 'reactstrap';
import {
    PagingState,
    CustomPaging,
} from '@devexpress/dx-react-grid';
import {Line} from 'react-chartjs-2';
import HistoryGrid from './historygrid'
import randomColorRGB from 'random-color-rgb'

class History extends Component {
    constructor(props, context) {
        super(props, context)
        this._data = props.data.data
    }
    /**
     * @returns Object
     */
    prepareRecentData() {
        let data = this._data.action_history.slice(-5)
        let returnData = {
            labels : data.map((history) => {
                let date = new Date(history.date)
                return date.getUTCDay()+"/"+date.getUTCDate()+"/"+date.getUTCFullYear()
            }),
            datasets : []
        }

        let dataByApp = {}
        for ( let d of data) {
            for (let request of d.requests) {
                if (!dataByApp[request.application]) {
                    if (!request.hasOwnProperty('application')) {
                        request.application = "No application"
                    }
                    dataByApp[request.application] = {}
                }
            }
        }
        for ( let d of data) {
            for (let application in dataByApp) {
                dataByApp[application][d.date] = []
            }
        }
        for ( let d of data) {
            for (let request of d.requests) {
                dataByApp[request.application][d.date].push(request)
            }
        }
        
        for (let application in dataByApp) {
            
            let newSet = {
                label : application,
                backgroundColor: randomColorRGB({opacity: 0.2}),
                data: Object.entries(dataByApp[application]).map(([k,v]) => {
                    return v.length
                })
            }
            returnData.datasets.push(newSet)
        }


        return returnData
    }

    prepareLastActivity() {
        console.log(this._data.action_history.length)
        if (this._data.action_history.length == 0) {
            return {}
        }
        let data = this._data.action_history[this._data.action_history.length-1].requests.slice(-10)
        let returnData = {}
        for (let req of data) {
            
            if (!req.application) {
                req.application = 'No application'
            }
            if (!returnData[req.application])
            returnData[req.application] = []
            
            returnData[req.application].push(req.execution_time)
        }
        let res = {
            labels : [],
            datasets : []
        }
        let size = 0

        for (let app in returnData) {
            if (returnData[app].length > size) {
                size = returnData[app].length
            }
        }
        
        for (let i = 0; i < size; i++) {
            res.labels.push(i)
        }

        
        for (let app in returnData) {
            res.datasets.push(
                {
                    label : app,
                    backgroundColor: randomColorRGB({opacity: 0.2}),
                    data: returnData[app].map((v) => {
                        return v
                    })
                }
            )
        }
        return res
    }

    getCharts() {   
        
        var dataRecent = this.prepareRecentData()
        var lastActivityExecution = this.prepareLastActivity()
        return (
            <React.Fragment>
                <Row className="edit-form">
                    <Col sm="6">
                        <Card body>
                            <CardTitle>Recent activity</CardTitle>
                            <Line ref="recent_act" data={dataRecent} width="450" height="250"/>
                        </Card>
                    </Col>
                    <Col sm="6">
                        <Card body>
                            <CardTitle>Execution Time of Last 10 Requests</CardTitle>
                            <Line ref="last_requests" data={lastActivityExecution} width="450" height="250"/>
                        </Card>
                    </Col>
                </Row>
                <Row>
                    <HistoryGrid data={this._data.action_history}/>
                </Row>
            </React.Fragment>
        )
    }
    render() {
        return this.getCharts()
    }
}

export default History