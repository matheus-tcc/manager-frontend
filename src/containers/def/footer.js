import React, { Component } from 'react';

class Footer extends Component {
    render() {
  
      // eslint-disable-next-line
      const { children, ...attributes } = this.props;
  
      return (
        <React.Fragment>
          <footer>
            <div className="footer-content">
              <span><a href="">API Manager</a></span>
              <span> </span>
              <span className="ml-auto">Powered by <a href="https://react.js">ReactJS</a></span>
            </div>
          </footer>
        </React.Fragment>
      );
    }
  }
  
  export default Footer;