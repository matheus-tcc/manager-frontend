import React, { Component } from 'react';
import '../../login.css'
import { Button, Row, Col } from 'reactstrap';
import { css } from 'react-emotion';
import { ClipLoader } from 'react-spinners'
import {
    DataTypeProvider,
  } from '@devexpress/dx-react-grid';
import { Grid, Table, TableHeaderRow, PagingPanel} from '@devexpress/dx-react-grid-bootstrap4';
import {
    PagingState,
    CustomPaging,
  } from '@devexpress/dx-react-grid';
import Session from '../../session'


class GridContainer extends Component {
    constructor(props, context) {
        super(props, context)
        this._sessionManager = new Session()
        this._booleanColumns = []
        this._prepareSelf()
        this.listener = window.addEventListener("hashchange", (ev) => {
            if (window.location.hash.indexOf("grid") !== -1) {
                this._prepareSelf()
            }
        })

        
    }
    componentWillUnmount() {
        window.removeEventListener("hashchange", this._listener)
    }
    _prepareSelf() {
        this.state = null
        this._gridData = null
        this.state = {
            totalCount: 0,
            pageSize: 10,
            currentPage: 1,
            loading: true,
        }
        this._sessionManager.getSessionId().then((id) => {
            this._sesionId = id
            this.fetchGridInfo()
            this.fetchData()
            this.changeCurrentPage = this.changeCurrentPage.bind(this);
        });
    }
    changeCurrentPage(currentPage) {
        this.setState({
          loading: true,
          currentPage,
        });
    }

    queryString() {
        const { pageSize, currentPage } = this.state;
        return `${this.routeToRequest}?limit=${pageSize}&page=${currentPage}`;
    }

    queryStringInfo() {
        return `${this.route_to_grid_info}?grid_id=${this.current_data_type}`;
    }

    fetchGridInfo() {
        const queryString = this.queryStringInfo()
        fetch(queryString, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'X-Api-Manager-Session' : this._sesionId
                },
                dataType: 'json',
            credentials: 'same-origin',
        }).then((response)=>{
            if (response.status === 403) {
                this._sessionManager.killSession()
            }
            if (response.status === 200) {
                response.json().then((data) => {
                    this._gridInfo = data
                    this.state.loading = false
                    this.fetchData()
                })
            }
        })
        
    }
    
    fetchData() {
        const queryString = this.queryString();
        
        if (queryString === this.lastQuery) {
            this.setState({ loading: false });
            return;
        }

        fetch(this.queryString(), {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'X-Api-Manager-Session' : this._sesionId
                },
                dataType: 'json',
            credentials: 'same-origin',
        }).then((response)=>{
            
            if (response.status === 200) {
                response.json().then((data) => {
                    this._gridData = data.data
                    this._index = data.index
                    this.state.totalCount = data.total_qty
                    this.state.currentPage = data.current_page - 1
                    this.state.pageSize = data.data.length
                    this.state.loading = false
                    this.forceUpdate()
                })
            }
        })

        this.lastQuery = queryString;
    }

    get current_data_type() {
        let splitStr = window.location.hash.split('/')
        return splitStr[splitStr.length-1]
    }

    get routeToRequest() {
        return global.config.api_manager_url+this.current_data_type
    }

    get route_to_grid_info() {
        return global.config.api_url+global.config.grid_info
    }

    get columns () {
        let columns = []
        let gridColumns = this._gridInfo['grid_columns']
        console.log(gridColumns)
        for (let attr of gridColumns) {
            if (attr.type === "boolean") {
                this._booleanColumns.push(attr.column_id)
            }
            columns.push({
                'name' : attr.column_id,
                'title' : attr.title
            })
        }

        columns.push({
            'name' : 'edit',
            'title' : "Edit"
        })

        return columns
    }

    get rows() {
        return this._gridData
    }
    createOrEdit(entityId) {
        let path = '/edit/entity/'+this.current_data_type
        if (entityId) {
            path = path+'/id/'+entityId
        }
        window.location.hash = path
    }
    render() {
        const {
            pageSize, currentPage, totalCount
        } = this.state;
        if (this._gridData && this._gridInfo) {
            for (let key in this._gridData) {
                this._gridData[key].edit = (<Button color="info" onClick={(ev) => {
                    this.createOrEdit(this._gridData[key][this._index])
                }}>Edit</Button>)
            }
            const BooleanFormatter = ({ value }) => (
                <span className="badge badge-secondary">
                  {value ? 'Yes' : 'No'}
                </span>
            );
            const BooleanTypeProvider = props => (
            <DataTypeProvider
                formatterComponent={BooleanFormatter}
                {...props}
            />
            );
            return (
                <Row className="grid-container">
                    <Col>
                        <div className="over-grid">
                            <div className="title-container">
                                <h3 className="grid-title">{this._gridInfo.title}</h3>
                            </div>
                            <div className="grid-button-container">
                                <Button color="danger" onClick={(event) => { this.createOrEdit() }}>Create New</Button>
                            </div>
                        </div>
                        <div className="grid-inner-container table-striped">
                            <Grid
                                rows={this.rows}
                                columns={this.columns}>
                                <BooleanTypeProvider
                                    for={this._booleanColumns}
                                />
                                <PagingState
                                    currentPage={currentPage}
                                    onCurrentPageChange={this.changeCurrentPage}
                                    pageSize={pageSize}
                                />
                                <CustomPaging
                                    totalCount={totalCount}
                                />
                                <Table />
                                <TableHeaderRow />
                                <PagingPanel />
                            </Grid>
                        </div>
                    </Col>
                </Row>
            )
        }
        return  this._loading()
    }
    _loading() {
        const override = css`
            display: block;
            margin: 0 auto;
            border-color: red;
        `;

        return (
            <div className='sweet-loading'>
              <ClipLoader
                className={override}
                sizeUnit={"px"}
                size={50}
                color={'#123abc'}
                loading={this._isLoaded}
              />
            </div> 
          )
    }
}

export default GridContainer;