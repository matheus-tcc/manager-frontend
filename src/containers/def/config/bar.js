import React, { Component } from 'react';
import { Collapse, Button, NavbarBrand,NavbarToggler,Navbar, Nav, NavItem, NavLink } from 'reactstrap';
import { ClipLoader } from 'react-spinners'
class Bar extends Component {
    constructor(props, config){
        super(props, config)
        this.title = props.title
        this.onSave = (ev) => {
            let element = ev.target
            this.state._isLoading = element.disabled = true
            this.setState(this.state)
            props.onSave(ev, () => {
                this.state._isLoading = element.disabled = false
                this.setState(this.state)
            })
        }
        this.toggle = this.toggle.bind(this)
        this.state = {
            isOpen : false,
            _isLoading: false
        }
    }
    toggle() {
        this.state.isOpen = !this.state.isOpen
        this.setState(this.state)
    }

    render() {
        return (
            <Navbar color="light" light expand="md" fixed="true">
                <NavbarBrand>{this.title}</NavbarBrand>
                <NavbarToggler onClick={this.toggle} />
                <Collapse isOpen={this.state.isOpen} navbar>
                    <Nav className="ml-auto" navbar>
                    <NavItem>
                      <div className='sweet-loading'>
                        <ClipLoader
                            sizeUnit={"px"}
                            size={20}
                            color={'#123abc'}
                            loading={this.state._isLoading}
                        />
                        </div> 
                    </NavItem>
                    <NavItem>
                        <Button onClick={this.onSave}>Save</Button>
                    </NavItem>
                    </Nav>
                </Collapse>
            </Navbar>
        )
    }
}

export default Bar;