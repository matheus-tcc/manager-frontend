import Login  from './containers/def/login'
import Edit  from './containers/def/edit'
import Config  from './containers/def/config' 
import Dashboard from './containers/def/dashboard';
import GridContainer from './containers/def/gridcontainer';

const routes = [
  { path: '/home', exact: true, name: 'Dashboard', component: Dashboard },
  { path: '/login', exact: true, name: 'Login', component: Login , full: true},
  { path: '/grid', exact: false, name: 'Grid', component: GridContainer },
  { path: '/edit', exact: false, name: 'Edit', component: Edit },
  { path: '/config', exact: false, name: 'Config', component: Config }
];

export default routes;