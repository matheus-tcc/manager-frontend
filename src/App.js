import React, { Component } from 'react';
import { css } from 'react-emotion';
import { ClipLoader } from 'react-spinners';
import empty from 'is-empty'
import logo from './logo.svg';
import config from './config.json'
import Footer from './containers/def/footer'
import Header from './containers/def/header'
import { combineReducers } from 'redux';
import { sessionReducer, sessionService } from 'redux-react-session';
import { createStore } from 'redux';
import './App.css';
import '../node_modules/rc-menu/assets/index.css';
import '../node_modules/react-toastify/dist/ReactToastify.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { ToastContainer} from 'react-toastify';
import routes from './routes'
import {Container} from 'reactstrap'


const NO_HEADER_AND_FOOTER  = 'noborder'

class App extends Component {
    
    constructor(){
        super()
        this.flags = []
        global.config = config
        this.initiateSession()
        this.state = this.getInitialState()
        
    }
    
    getInitialState() {
        return {
            route: window.location.hash.substr(1),
            loading:true
        }
    }
    componentWillUpdate() {
        this.flags = []
        return true
    }

    componentDidMount() {
        window.addEventListener('hashchange', () => {
            this.flags = []
            this.setState({
                route: window.location.hash.substr(1)
            })
            this.initiateSession().then(() => {
                sessionService.loadSession().then((res) => {
                    this.sessionObj = res
                    document.dispatchEvent(new Event('session_loaded'))
                }).catch((err) => {
                    if (this.state.route != "login") {
                        window.location.hash = 'login'
                    }
                    this.sessionObj = null  
                    document.dispatchEvent(new Event('session_loaded'))
                })
            }).catch((err) => {
                console.log(err)
            })
            this.forceUpdate()
        })
        
        return true
    }

    componentWillMount() {
        document.addEventListener('session_loaded', () => {
            this.forceUpdate()
        })

        this.initiateSession().then(() => {
            sessionService.loadSession().then((res) => {
                this.sessionObj = res
                document.dispatchEvent(new Event('session_loaded'))
            }).catch((err) => {
                if (this.state.route != "login") {
                    window.location.hash = 'login'
                }
                this.sessionObj = null
                document.dispatchEvent(new Event('session_loaded'))
            })
        })
    }
    
  
    getState() {
        let result = this.state
        if (!result) {
           return this.getInitialState()
        }
        return result
    }
    
    
    async initiateSession() {
        const reducers = {
            session: sessionReducer
        };
        if (!this.reducer) {
            this.reducer = combineReducers(reducers);
        }
        if (!this.store) {
            this.store = createStore(this.reducer)
        }

        const options = { refreshOnCheckAuth: true, redirectPath: '/login', driver: 'COOKIES', 
            validateSession(data) {
                if (data.invalid) {
                    return false
                }

                return true
            }
        };

        await sessionService.initSessionService(this.store, options)
        
    }

 

    render() {
        console.log(this.state, this.sessionObj)
        let Child
        if (typeof this.sessionObj == 'undefined') {
            return this._loading()
        }
        
        var routeChosen = this.sessionObj == null ?  'login' : 'home'

        routes.map( (route) => {
            if (!empty(this.getState().route) && this.sessionObj ) {
                routeChosen = this.getState().route
            }
            if (routeChosen.indexOf(route.path) != -1 || route.path.indexOf(routeChosen) != -1) {
                Child = route.component
                if (route.full) {
                    this.flags.push(NO_HEADER_AND_FOOTER)
                }
                
            }
        })
        
        
        let Content 

        if ( this.flags.includes(NO_HEADER_AND_FOOTER) ) {
            Content = (<div className="app-login"><Child /></div>)
        }
        else {
            Content = (
                <div className="app"> 
                    <Header/>
                    <Container fluid className="app-content">
                        <Child />
                        <div className="clear"></div>
                    </Container>
                    <Footer />
                </div>)
        }
    
        return (
            <div className="app-content-container">
                <ToastContainer />
                {Content}
            </div>
        );
    }

    _loading() {
        const override = css`
            display: block;
            margin: 0 auto;
            border-color: red;
        `;

        return (
            <div className='sweet-loading'>
              <ClipLoader
                className={override}
                sizeUnit={"px"}
                size={150}
                color={'#123abc'}
                loading={this.getState().loading}
              />
            </div> 
          )
    }
}

export default App;
 